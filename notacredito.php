<?php
    ini_set('max_execution_time', 3000);

    //$fecha = date("Ymd", strtotime($_POST['fecha']));  

    //Limpiamos la carpeta RESULTADOS
    $dir = 'RESULTADOS/';     
    $handle = opendir($dir);
    $ficherosEliminados = 0;
    while ($file = readdir($handle)) {
        if (is_file($dir.$file)) {
            unlink($dir.$file);
        }
    }

    $boletas = ['692082'];

    for ($i=0; $i < count($boletas); $i++) { 
        generar($boletas[$i]);
    }

    function generar($boleta) {
        $serverName = "202.15.1.14";
        $database = "SIGH";
        $uid = 'User_FactESeguro';
        $pwd = 'HnalFac$18%Sguro';
        #$idComprobantePago = '8';

        try {
            $conn = new PDO(
                "sqlsrv:server=$serverName;Database=$database",
                $uid,
                $pwd,
                array(
                    //PDO::ATTR_PERSISTENT => true,
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                )
            );
        }
        catch(PDOException $e) {
            die("Error connecting to SQL Server: " . $e->getMessage());
        } 
        $trama = null;
        $trama = new stdClass();
        $cabecera = 'exec SIGESA_TRAMA_CABECERA_NOTA_X_CODIGO ' . $boleta;
        $stmt = $conn->query( $cabecera );
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){ $data[] = $row; }
        $stmt = null;

        $detalle = 'exec SIGESA_TRAMA_DETALLE_X_CODIGO ' . $boleta;
        $stmt = $conn->query( $detalle );
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){ $detalles[] = $row; }

        $leyenda[] = array(
                    'codigo'      => '1000', 
                    'descripcion'   => ''
            );

        $totalImpuestos[] = array(
            'idImpuesto'      => $data[0]['CABtotalImpidImpuesto'], 
            'montoImpuesto'   => number_format($data[0]['CABtotalImpmontoImpuesto'],2,'.',' ')
        );
        $totalImpustosDocumento=0;
        for ($i=0; $i < count($detalles); $i++) { 

            $totalImpuestos2[0] = array(
                'idImpuesto'        =>  $data[0]['CABtotalImpidImpuesto'],  
                'montoImpuesto'     =>  number_format($detalles[$i]['TOTALIMPUESTO'],2, '.', ' '), 
                'tipoAfectacion'    =>  '30',  //10 GRAVADAS 30 INAFECTAS
                'montoBase'         => number_format($detalles[$i]['TOTALPAGAR']-$detalles[$i]['TOTALIMPUESTO'],2,'.',' '),
                'porcentaje'        => '0' //SI ES INAFECTA QUE SE MANTENGA EN 0
            );

            $data_detalles[] = array(
                'numeroItem'            => strval('00' . $i + 1), 
                'codigoProducto'        => $detalles[$i]['IDPRODUCTO'], 
                'descripcionProducto'   => $detalles[$i]['NOMBRE'], 
                'cantidadItems'         => $detalles[$i]['CANTIDADPAGAR'], 
                'unidad'                => 'NIU', 
                'valorUnitario'         => number_format($detalles[$i]['VALORUNITARIO'],2,'.',' '), 
                'precioVentaUnitario'   => number_format($detalles[$i]['PRECIOVENTA'],2,'.',' '),
                'totalImpuestos'        => $totalImpuestos2,
                'valorVenta'            => number_format($detalles[$i]['TOTALPAGAR'],2,'.',' '),
                'montoTotalImpuestos'   => number_format($detalles[$i]['TOTALIMPUESTO'],2, '.', ' ')    
            );

            $drf[] = array(
                'tipoDocRelacionado'    =>  $data[0]['DRFtipoDocRelacionado'],
                'numeroDocRelacionado'  =>  $data[0]['DRFnumeroDocRelacionado'],
                'codigoMotivo'          =>  $data[0]['DRFcodigoMotivo'],
                'descripcionMotivo'     =>  $data[0]['DRFdescripcionMotivo']
            );

            
            $totalImpustosDocumento = $totalImpustosDocumento+number_format($detalles[$i]['TOTALIMPUESTO'],2, '.', ' '); 
        }
        
       /* if ($data[0]['CODTIPODOCUMENTO'] < 10) {
            $codTipoDocumento = '0' . $data[0]['CODTIPODOCUMENTO'];
        } else {
            $codTipoDocumento = $data[0]['CODTIPODOCUMENTO'];
        }*/

        $trama->notaCredito = array(
            'IDE' => array(
                'numeracion'        => $data[0]['IDEnumeracion'],
                'fechaEmision'      => $data[0]['IDEfechaEmision'],
                'horaEmision'       => $data[0]['IDEhoraEmision'],
                //'codTipoDocumento'  => $codTipoDocumento,
                'tipoMoneda'        => $data[0]['IDEtipoMoneda'] 
            ),
            'EMI' => array(
                'tipoDocId'     => trim($data[0]['TIPODOCIDEMI']),
                'numeroDocId'   => $data[0]['NUMERODOCIDEMI'],
                'razonSocial'   => $data[0]['RAZONSOCIALEMI'],
                'direccion'     => $data[0]['DIRECCIONEMI'],
                'codigoAsigSUNAT' => '0000'
            ),
            'REC' => array(
                'tipoDocId'     => $data[0]['TIPODOCREC'],
                'numeroDocId'   => $data[0]['NUMERODOCIDREC'],
                'razonSocial'   => $data[0]['RAZONSOCIALREC'],
                'direccion'     => $data[0]['DIRECCIONREC']
            ),
            'DRF' => $drf,
            'CAB' => array(
                'inafectas' => array(   //hacer que dependa de una variable si es grabada o inafecta
                    'codigo'        => $data[0]['CABinafcodigo'], 
                    'totalVentas'   => number_format($data[0]['CABinaftotalVentas'],2,'.',' ')
                ),
                'totalImpuestos' => $totalImpuestos,
                'importeTotal' => number_format($data[0]['CABimporteTotal'],2,'.',' '),
                //'tipoOperacion' => '0101',  //boleta de venta
                'montoTotalImpuestos' => number_format($totalImpustosDocumento,2,'.',' ')

                //'leyenda' => $leyenda
            ),
            'DET' => $data_detalles
        );

        

        

        // revisar manual tecnico
        header("Content-type: application/json; charset=utf-8");
        $jsonencoded = json_encode($trama,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        //echo base64_encode($jsonencoded);
        file_put_contents('RESULTADOS/' . $data[0]['NUMERODOCIDEMI'] . '-07-' . $data[0]['IDEnumeracion'] . '.json', $jsonencoded);
        
        $data_array =  array(
            'customer'        =>  array('username'      =>      '20154996991loayza02',
                                          'password'      =>    'Loayza2018*'
                    ),
              'fileName'        =>   $data[0]['NUMERODOCIDEMI'] . '-07-' . $data[0]['IDEnumeracion'] . '.json',
              'fileContent'     =>   base64_encode($jsonencoded)
        );

        echo json_encode($data_array,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);;
/*
        $make_call = callAPI('POST', 'https://api.example.com/post_url/', json_encode($data_array));
        $response = json_decode($make_call, true);
        $errors   = $response['response']['errors'];
        $data     = $response['response']['data'][0];
*/

        //PARA SUBIR DIRECTO AL SFTP!!
        //subir('RESULTADOS/' . $data[0]['NUMERODOCIDEMI'] . '-03-' . $data[0]['NUMERACION'] . '.json');

        $jsonencoded = null;
        
    }

    function subir($documento)
    {

        $dataFile      = $documento;
        $sftpServer    = 'sftpqa.escondatagate.net';
        $sftpUsername  = 'loayza02';
        $sftpPassword  = 'Loayza2018*';
        $sftpPort      = 3022;
        $sftpRemoteDir = '/WWW/entrada';
         
        $ch = curl_init('sftp://' . $sftpServer . ':' . $sftpPort . $sftpRemoteDir . '/' . basename($dataFile));
         
        $fh = fopen($dataFile, 'r');
         
        if ($fh) {
            curl_setopt($ch, CURLOPT_USERPWD, $sftpUsername . ':' . $sftpPassword);
            curl_setopt($ch, CURLOPT_UPLOAD, true);
            curl_setopt($ch, CURLOPT_PROTOCOLS, CURLPROTO_SFTP);
            curl_setopt($ch, CURLOPT_INFILE, $fh);
            curl_setopt($ch, CURLOPT_INFILESIZE, filesize($dataFile));
            curl_setopt($ch, CURLOPT_VERBOSE, true);
         
            $verbose = fopen('php://temp', 'w+');
            curl_setopt($ch, CURLOPT_STDERR, $verbose);
         
            $response = curl_exec($ch);
            $error = curl_error($ch);
            curl_close($ch);
         
            if ($response) {
                echo "Success";
            } else {
                echo "Failure";
                rewind($verbose);
                $verboseLog = stream_get_contents($verbose);
                echo "Verbose information:\n" . $verboseLog . "\n";
            }
        }

    }

    $stmt = null;
    $conn = null;

    exec("explorer.exe RESULTADOS");

    //header('Location:index.php');
?>