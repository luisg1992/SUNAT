<?php
    ini_set('max_execution_time', 3000);

    $idcomprobante = $_POST['idcomprobante'];  
    $errores = 0;
    $correctos = 0;
    //Limpiamos la carpeta RESULTADOS

    $dir = 'RESULTADOS/';     
    $handle = opendir($dir);
    $ficherosEliminados = 0;
    while ($file = readdir($handle)) {
        if (is_file($dir.$file)) {
            unlink($dir.$file);
        }
    }
    
    generar($idcomprobante);
    

    function generar($boleta) { 
        $serverName = "202.15.1.14";
        $database = "SIGH";
        $uid = 'User_FactESeguro';
        $pwd = 'HnalFac$18%Sguro';
        #$idComprobantePago = '8';

        try {
            $conn = new PDO(
                "sqlsrv:server=$serverName;Database=$database",
                $uid,
                $pwd,
                array(
                    //PDO::ATTR_PERSISTENT => true,
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                )
            );
        }
        catch(PDOException $e) {
            die("Error connecting to SQL Server: " . $e->getMessage());
        } 
        $trama = null;
        $trama = new stdClass();
        $cabecera = 'exec SIGESA_TRAMA_CABECERA_X_CODIGO ' . $boleta;
        $stmt = $conn->query( $cabecera );
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){ $data[] = $row; }
        $stmt = null;

        $detalle = 'exec SIGESA_TRAMA_DETALLE_X_CODIGO ' . $boleta;
        $stmt = $conn->query( $detalle );
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){ $detalles[] = $row; }

        $leyenda[] = array(
                    'codigo'      => '1000', 
                    'descripcion'   => ''
            );

        $totalImpuestos[] = array(
            'idImpuesto'      => $data[0]['CODIGO_IMPUESTO'], 
            'montoImpuesto'   => number_format($data[0]['TOTAL_IMPUESTO'],2,'.',' ')
        );
        $totalImpustosDocumento=0;
        $ValorTotal = 0;    
        for ($i=0; $i < count($detalles); $i++) { 

            if ($data[0]['CAB']=='INAFECTAS') {
                $tipoAfectacion = "30";
                $porcentaje = "0";
            }else{
                $tipoAfectacion = "10";
                $porcentaje = "18";
            }

            $totalImpuestos2[0] = array(
                'idImpuesto'        =>  $data[0]['CODIGO_IMPUESTO'], 
                'montoImpuesto'     =>  number_format($detalles[$i]['TOTALIMPUESTO'],2, '.', ' '), 
                'tipoAfectacion'    =>  $tipoAfectacion,
                'montoBase'         =>  number_format($detalles[$i]['TOTALPAGAR']-$detalles[$i]['TOTALIMPUESTO'],2,'.',' '),
                'porcentaje'        => $porcentaje
            );

            $data_detalles[] = array(
                'numeroItem'            => strval('00' . $i + 1), 
                'codigoProducto'        => $detalles[$i]['IDPRODUCTO'], 
                'descripcionProducto'   => $detalles[$i]['NOMBRE'], 
                'cantidadItems'         => $detalles[$i]['CANTIDADPAGAR'], 
                'unidad'                => 'NIU', 
                'valorUnitario'         => number_format($detalles[$i]['VALORUNITARIO'],2,'.',' '), 
                'precioVentaUnitario'   => number_format($detalles[$i]['PRECIOVENTA'],2,'.',' '),
                'totalImpuestos'        => $totalImpuestos2,
                'valorVenta'            => number_format($detalles[$i]['VALORUNITARIO']* $detalles[$i]['CANTIDADPAGAR'],2,'.',' ') , //number_format($detalles[$i]['TOTALPAGAR'],2,'.',' '),
                'montoTotalImpuestos'   => number_format($detalles[$i]['TOTALIMPUESTO'],2, '.', ' ')    
            );

            $ValorTotal = $ValorTotal + ($detalles[$i]['PRECIOVENTA'] * $detalles[$i]['CANTIDADPAGAR']);

            $totalImpustosDocumento = $totalImpustosDocumento+number_format($detalles[$i]['TOTALIMPUESTO'],2, '.', ' '); 
        }
        
        if ($data[0]['CODTIPODOCUMENTO'] < 10) {
            $codTipoDocumento = '0' . $data[0]['CODTIPODOCUMENTO'];
        } else {
            $codTipoDocumento = $data[0]['CODTIPODOCUMENTO'];
        }

        $trama->boleta = array(
            'IDE' => array(
                'numeracion'        => $data[0]['NUMERACION'],
                'fechaEmision'      => $data[0]['FECHAEMISION'],
                'horaEmision'       => $data[0]['HORAEMISION'],
                'codTipoDocumento'  => $codTipoDocumento,
                'tipoMoneda'        => $data[0]['TIPOMONEDA'] 
            ),
            'EMI' => array(
                'tipoDocId'     => trim($data[0]['TIPODOCIDEMI']),
                'numeroDocId'   => $data[0]['NUMERODOCIDEMI'],
                'razonSocial'   => $data[0]['RAZONSOCIALEMI'],
                'direccion'     => $data[0]['DIRECCIONEMI'],
                'codigoAsigSUNAT' => '0000'
            ),
            'REC' => array(
                'tipoDocId'     => $data[0]['TIPODOCRELACIONADO'],
                'numeroDocId'   => $data[0]['NUMERODOCID'],
                'razonSocial'   => $data[0]['RAZONSOCIAL'],
                'direccion'     => $data[0]['DIRECCION']
            ),
            'CAB' => array(
                strtolower($data[0]['CAB']) => array(
                    'codigo'        => $data[0]['CAB_CODIGO'], 
                    'totalVentas'   => number_format($data[0]['CAB_TOTALVENTAS'],2,'.',' ')
                ),
                'totalImpuestos' => $totalImpuestos,
                'importeTotal' => number_format($data[0]['IMPORTE_TOTAL'],2,'.',' '), //number_format($ValorTotal,2,'.',' '),
                'tipoOperacion' => '0101',  //boleta de venta
                'montoTotalImpuestos' => number_format($totalImpustosDocumento,2,'.',' ')

                //'leyenda' => $leyenda
            ),
            'DET' => $data_detalles
        );

        

        

        // revisar manual tecnico
        header("Content-type: application/json; charset=utf-8");
        $jsonencoded = json_encode($trama,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        file_put_contents('RESULTADOS/' . $data[0]['NUMERODOCIDEMI'] . '-03-' . $data[0]['NUMERACION'] . '.json', $jsonencoded);
        
        $data_array =  array(
            'customer'        =>  array('username'      =>      '20154996991loayza02',
                                          'password'      =>    'Loayza2018*'
                    ),
              'fileName'        =>   $data[0]['NUMERODOCIDEMI'] . '-03-' . $data[0]['NUMERACION'] . '.json',
              'fileContent'     =>   base64_encode($jsonencoded)
        );

        echo json_encode($data_array,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);;

    
        $make_call = callAPI(json_encode($data_array));
        $response = json_decode($make_call, true);
        $errors   = $response['response']['errors'];
        $data     = $response['response']['data'][0];
/*
        echo $errors;
        echo $data;
*/
        //PARA SUBIR DIRECTO AL SFTP!!
        //subir('RESULTADOS/' . $data[0]['NUMERODOCIDEMI'] . '-03-' . $data[0]['NUMERACION'] . '.json');

        $jsonencoded = null;
        
    }

    //SUBIT A FTP
    function subir($documento)
    {

        $dataFile      = $documento;
        $sftpServer    = 'sftpescondatagate.net';
        $sftpUsername  = 'loayza02';
        $sftpPassword  = 'Loayza2018*';
        $sftpPort      = 3022;
        $sftpRemoteDir = '/WWW/entrada';
         
        $ch = curl_init('sftp://' . $sftpServer . ':' . $sftpPort . $sftpRemoteDir . '/' . basename($dataFile));
         
        $fh = fopen($dataFile, 'r');
         
        if ($fh) {
            curl_setopt($ch, CURLOPT_USERPWD, $sftpUsername . ':' . $sftpPassword);
            curl_setopt($ch, CURLOPT_UPLOAD, true);
            curl_setopt($ch, CURLOPT_PROTOCOLS, CURLPROTO_SFTP);
            curl_setopt($ch, CURLOPT_INFILE, $fh);
            curl_setopt($ch, CURLOPT_INFILESIZE, filesize($dataFile));
            curl_setopt($ch, CURLOPT_VERBOSE, true);
         
            $verbose = fopen('php://temp', 'w+');
            curl_setopt($ch, CURLOPT_STDERR, $verbose);
         
            $response = curl_exec($ch);
            $error = curl_error($ch);
            curl_close($ch);
         
            if ($response) {
                echo "Success";
            } else {
                echo "Failure";
                rewind($verbose);
                $verboseLog = stream_get_contents($verbose);
                echo "Verbose information:\n" . $verboseLog . "\n";
            }
        }

    }

    /************************************PROBANDO API REST****************************************/
    function callAPI($data, $idComprobante){         
        //API Url
        $url = 'http://calidad.escondatagate.net/wsParser_2_1/rest/parserWS ';
         
        //Initiate cURL.
        $ch = curl_init($url);
         
        //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POST, 1);
         
        //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        //Acivamos la funcion para que nos muestre el resultado
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         
        //Set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
         
        //Execute the request
        $result = curl_exec($ch);

        $respuesta = json_decode($result);

        //echo $result;
        if ($respuesta->responseCode != 0) {
            $estado = 0;
            $GLOBALS['errores']++;
        }else{
            $estado = 1;
            $GLOBALS['correctos']++;
        }

        actualizarEstado($idcomprobante, $estado);

    }

    function actualizarEstado ($idComprobante, $estado)
    {
        $serverName = "202.15.1.14";
        $database = "SIGH";
        $uid = 'User_FactESeguro';
        $pwd = 'HnalFac$18%Sguro';
        #$idComprobantePago = '8';

        try {
            $conn = new PDO(
                "sqlsrv:server=$serverName;Database=$database",
                $uid,
                $pwd,
                array(
                    //PDO::ATTR_PERSISTENT => true,
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                )
            );
        }
        catch(PDOException $e) {
            die("Error connecting to SQL Server: " . $e->getMessage());
        } 
        $trama = null;
        $trama = new stdClass();
        $cabecera = 'exec SIGESA_TRAMA_CABECERA_X_CODIGO ' . $boleta;
        $stmt = $conn->query( $cabecera );
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){ $data[] = $row; }
        $stmt = null;
    }
/*********************************************************************************************************/

    $stmt = null;
    $conn = null;

    exec("explorer.exe RESULTADOS");
  //  header('Location:index.php?c='.$correctos.'&e='.$errores);
?>