<?php 

	if (isset($_GET['c'])&&isset($_GET['e'])) {
    	$correctos = $_GET['c'];
		$errores = $_GET['e'];

	}else{
		
	}

 ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset='UTF-8'>
	<title>SUNAT-HNAL</title>
	<link rel="icon" type="image/png" href="sunat.png" />
	<link rel="stylesheet" href="css/index.css">
	<link rel="stylesheet" href="css/sweetalert2.min.css">
	<script src="js/jquery-3.3.1.min.js"></script>
	<script src="js/sweetalert2.all.min.js"></script>


</head>
<body>
<div class="login">
	<h1>Generar Tramas Sunat JSON</h1>
    <form method="POST" id="myForm">
    	<input type="date" name="fecha" id="fecha" style="text-align:center; font-size: 24px;	" />
<!--        <button type="submit" class="btn btn-primary btn-block btn-large">Generar</button>  -->
        <button id="button1" class="btn btn-primary btn-block btn-large">Boletas y Facturas</button></br>
        <button id="button2" class="btn btn-primary btn-block btn-large">Notas de Credito</button>
        <br>
        <input type="text" name="idcomprobante" id="idcomprobante" placeholder="IDCOMPROBANTE" style="text-align: center; font-size: 24px; " />
        <button id="button3" class="btn btn-primary btn-block btn-large">COMPROBANTE INDIVIDUAL</button>

    </form>
</div>
<div id="myDiv">
        <img id="loading-image" src="ajax-loader.gif" height="150" width="150" class="hideimage" />
    </div>
</body>
</html>

<script type="text/javascript">
	$('#button1').click(function(event){
	   //$('#myForm').attr('action', 'operacion.php');
	   event.preventDefault();

	   $.ajax({
	   		type: "POST",
            url: "operacion.php",
            data: { fecha: $("#fecha").val() },
            beforeSend: function() {
              $("#loading-image").show();//.removeClass('hideimage').addClass('showimage');
           },
            success:function(){ 
      		  $("#loading-image").hide();
    		}});
	   //alert('boletas');
	   $("#loading-image").hide();
	});


	$('#button2').click(function(event){
	   //$('#myForm').attr('action', 'notacredito.php');
	   event.preventDefault();

	   $.ajax({
	   		type: "POST",
            url: "notacredito.php",
            data: { fecha: $("#fecha").val() },
            beforeSend: function() {
            	alert();
              //$("#loading-image").removeClass('hideimage').addClass('showimage');
           },
            success:function(result){ 
      			//$("#loading-image").hide();
    		}});
	   //alert('notas');
	});

	$('#button3').click(function(event){
	   //$('#myForm').attr('action', 'individual.php');

	   $.ajax({
	   		type: "POST",
            url: "individual.php",
            data: { idcomprobante: $("#idcomprobante").val() },
            beforeSend: function() {

           },
            success:function(result){
            	alert(result);
    		}
    	});

	});	

	var correctos = getParameterByName('c');
	var errores = getParameterByName('e');

	if (!!correctos && !!errores && correctos!=0 && errores!=0) {
		swal(
		  'Good job!',
		  'Envios correctos:&nbsp;&nbsp;&nbsp;'+correctos+ '<br><br>Envios Erroneos:&nbsp;&nbsp;&nbsp;'+errores,
		  'success'
		)
	}
	else{
		
	}

	function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
	

</script>